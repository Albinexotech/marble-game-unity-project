using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class LevelTimer : TimerBase
{
    // Methods
    // Called on start
    protected override void Start()
    {
        base.Start();
        TimerName = "Level";
    }
    // Called when the timer is toggled
    public override void OnToggle()
    {
        // If we are unpausing, we reset the timer
        if(IsTimerPaused)
        {
            Seconds = 0;
            Minutes = 0;
        }
        // Finally, call the original function
        base.OnToggle();
    }
}
