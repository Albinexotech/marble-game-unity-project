using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalTimer : TimerBase 
{
    // Methods
    // Called when the timer is toggled
    public override void OnToggle()
    {
        // Store our current times to the class
       DataStoreCurrentTime.CurrentMinutes = Minutes;
       DataStoreCurrentTime.CurrentSeconds = Seconds;
        base.OnToggle();
    }
}
