using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using TMPro;

public class CheckBestTime : MonoBehaviour
{
    // Fields / Properties
    //Timer text
    private TextMeshProUGUI _text;
    
    // Methods
    // Called when object starts (before first frame)
    void Start()
    {
        _text = GetComponent<TextMeshProUGUI>();
        // If an older time already exists, load it
        if(File.Exists(Application.persistentDataPath + "/bestTotal.time"))
        {
            // Setup the BinaryFormatter, which serializes data in binary form
            BinaryFormatter _binaryFormatter = new BinaryFormatter();
            // Setup the filestream, which allows reading to and from our filepath. We want to read from it
            FileStream _fileStream = File.Open(Application.persistentDataPath + "/bestTotal.time", FileMode.Open);
            // Now, a class in DataStoreTime.cs is used to load the data from the file. We deserialize our file into binary form, and load it into DataStoreBestTime
            DataStoreBestTime _bestTime = (DataStoreBestTime)_binaryFormatter.Deserialize(_fileStream);
            // Make sure to close the filestream
            _fileStream.Close();
            // If the data we just loaded is not null, we can finally do the next check
            if(_bestTime != null)
            {
                // If the other times are not set yet, write the line right now
                if(DataStoreCurrentTime.CurrentMinutes != 0 && DataStoreCurrentTime.CurrentSeconds != 00.00f)
                {
                    CheckNewTime(_bestTime.BestMinutes, _bestTime.BestSeconds);
                }
                else
                {
                    _text.text = string.Format("Best Time: {0}:{1:#00.00}", _bestTime.BestMinutes, _bestTime.BestSeconds);
                }
            }
            // If that failed, say the file is corrupt
            else
            {
                _text.text = "Saved time cannot be read, and may be corrupt";
            }
        }
        // If that failed, set a default time and save that
        else
        {
            _text.text = "Best time will be shown here";
            SaveNewTime(99, 99.99f);
        }
    }
    // Now, we'll check if the current time is better or not. If it is, we'll save over the time. Finally, the text will be written
    void CheckNewTime(int _bestMinutes, float _bestSeconds)
    {
        // Get our current times that were last used
        int _currentMinutes = DataStoreCurrentTime.CurrentMinutes;
        float _currentSeconds = DataStoreCurrentTime.CurrentSeconds;
        // Start comparing: are the minutes better? We'll use a switch statement, to reduce lengthy if statements
        switch(_currentMinutes)
        {
            // If it is faster, our whole time is faster
            case int i when i < _bestMinutes:
                _bestMinutes = _currentMinutes;
                _bestSeconds = _currentSeconds;
                SaveNewTime(_bestMinutes, _bestSeconds);
                break;
            // If it is the same, we need to check the seconds
            case int i when i == _bestMinutes:
                // If the seconds are better, our time is better. If not, we pass
                if(_currentSeconds < _bestSeconds)
                {
                    _bestMinutes = _currentMinutes;
                    _bestSeconds = _currentSeconds;
                    SaveNewTime(_bestMinutes, _bestSeconds);
                }
                break;
            // If slower, we pass. This isn't really needed, and can be removed if desired
            default:
                break;
        }
        // Now that we checked if the best time needs to be updated, we finally update the text
        _text.text = string.Format("Best Time: {0}:{1:#00.00}", _bestMinutes, _bestSeconds);
    }
    // If the new time was updated, we need to save it into the file
    void SaveNewTime(int _bestMinutes, float _bestSeconds)
    {
        // This process is similar to loading the time
        // Start with the BinaryFormatter
        BinaryFormatter _binaryFormatter = new BinaryFormatter();
        // Setup the same filestream. This time, we are creating a file at the path
        FileStream _fileStream = File.Create(Application.persistentDataPath + "/bestTotal.time");
        // We use the same class as before, only now we need to store our time in it first
        DataStoreBestTime _bestTime = new DataStoreBestTime();
        _bestTime.BestMinutes = _bestMinutes;
        _bestTime.BestSeconds = _bestSeconds;
        // Now, we serialize our binary data into the filepath
        _binaryFormatter.Serialize(_fileStream, _bestTime);
        // Finally, close the FileStream
        _fileStream.Close();
    }
}
