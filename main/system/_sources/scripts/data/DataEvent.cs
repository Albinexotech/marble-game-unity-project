using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Event", fileName = "New Event")]
public class DataEvent : ScriptableObject
{
    // Private variables
    private List<DataEventListener> _eventListeners = new List<DataEventListener>();
    
    // Functions
    // Trigger the event, by calling each object in the List
    public void Trigger()
    {
        for (int i = _eventListeners.Count - 1; i >= 0; i--)
        {
            _eventListeners[i].RaiseEvent();
        }
    }
    
    // Add the event listener to the List
    public void AddListener(DataEventListener value)
    {
        _eventListeners.Add(value);
    }
    
    // Remove the event listener from the List
    public void RemoveListener(DataEventListener value)
    {
        _eventListeners.Remove(value);
    }
}
