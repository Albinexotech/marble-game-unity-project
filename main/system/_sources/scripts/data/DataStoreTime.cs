using System.Collections;
using System.Collections.Generic;

// Let's store two classes into the one script file. This class will be to store the current time, while the other will store the best time. They are used differently, but store very similar data. Also, neither use UnityEngine specific libraries, and are not attached to a GameObject directly
// Store the current time. This is static, so can be accessed easily. However, it is not persistent after the game closes
public static class DataStoreCurrentTime
{
    // Fields / Properties
    // Current Minutes and Seconds
    public static int CurrentMinutes;
    public static float CurrentSeconds;
}
// Store the best time. This is serialized, and is used for when we save/load the best time. This is destroyed when the scene is unloaded, but we don't need it at that point
[System.Serializable]
public class DataStoreBestTime
{
    // Fields / Properties
    public int BestMinutes;
    public float BestSeconds;
}
