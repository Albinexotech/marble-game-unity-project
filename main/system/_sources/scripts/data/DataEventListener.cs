using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DataEventListener : MonoBehaviour
{
    // Private Variables
    // The scriptable object event, referenced
    [SerializeField] 
    private DataEvent[] _dataEvent;
    // The UnityEvent from the script requiring the object
    [SerializeField] 
    private UnityEvent _unityEvent;
    
    // When the script is active, add this listener to the event
    private void OnEnable()
    {   
        for (int i = 0; i < _dataEvent.Length; i++)
        {
            _dataEvent[i].AddListener(this);
        }
    }
    
    // When the script is disabled, remove the listener from the event
    private void OnDisable()
    {
        for (int i = 0; i < _dataEvent.Length; i++)
        {
            _dataEvent[i].RemoveListener(this);
        }
    }
    
    // When the event is triggered via the scriptable object, invoke the attached UnityEvent, and run the event
    public void RaiseEvent()
    {
        _unityEvent.Invoke();
    }
}
