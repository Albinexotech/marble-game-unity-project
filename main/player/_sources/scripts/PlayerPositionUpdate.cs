using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerPositionUpdate : MonoBehaviour
{
    // Variables
    // A ScriptableObject to store values
    [SerializeField]
    private DataPlayerTransform _playerTransform;
    // The Respawn event to call
    [SerializeField]
    private DataEvent _respawnEvent;
    
    // UnityEvent for when the y-axis needs to reset
    public UnityEvent YAxisReset;
    
    // Functions
    // Called on every frame
    void FixedUpdate()
    {
        // Update the stored player position
        _playerTransform.PlayerPosition = transform.localPosition;
    }
    
    // If the player touches water, restart the level
    void OnTriggerEnter(Collider _collider)
    {
        if(_collider.tag == "Water")
        {
            RestartLevel();
        }
    }
    
    // Restart the level
    void RestartLevel()
    {
        // Reset the position, set velocity to 0, then call the Respawn event
        transform.position = _playerTransform.PlayerRestartPosition;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        _respawnEvent.Trigger();
    }
    
    // When the Y-axis of the player needs to reset
    public void PlayerResetYAxis()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 10.0f, transform.position.z);
        _playerTransform.PlayerPosition = transform.position;
    }
}
