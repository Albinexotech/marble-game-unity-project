using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Actor : MonoBehaviour
{
    // Fields / Properties
    // Is this actor active?
    [SerializeField]
    protected bool IsActorActive;
    // Original state at start
    protected bool WasActorActive;
    // When the button was pushed
    public UnityEvent ActorStateEvent;
    // When the player respawns
    public UnityEvent PlayerRespawnedEvent;
    
    // Methods
    // Called when started. This may be overriden by the inherited class
    protected virtual void Start()
    {
        // Add our listeners
        ActorStateEvent.AddListener(OnStateChanged);
        PlayerRespawnedEvent.AddListener(OnPlayerRespawn);
        // Store our actor state at start
        WasActorActive = IsActorActive;
    }
    // Called when the state is changed
    public virtual void OnStateChanged()
    {
        // Invert our actor state
        IsActorActive = !IsActorActive;
    }
    // Called when the player respawns
    public virtual void OnPlayerRespawn()
    {
        // Set the actor state to what it was at the start
        IsActorActive = WasActorActive;
    }
}
