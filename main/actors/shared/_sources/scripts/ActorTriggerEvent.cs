using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActorTriggerEvent : MonoBehaviour
{
    // Fields / Properties
    // The event to call when the collision is triggered
    public DataEvent ActorDataEvent;
    // What tag should the collider be checking for?
    [SerializeField]
    private string _tagName = "Button";
    // Should leaving the collision also trigger the event?
    [SerializeField]
    private bool _shouldLeavingTrigger = false;

    // Methods
    // Called when something enters the collider
    void OnTriggerEnter(Collider _collider)
    {
        // If the object has the right tag, trigger the event
        if(_collider.tag == _tagName)
        {
            ActorDataEvent.Trigger();
        }
    }
    // Called when something leaves the collider
    void OnTriggerExit(Collider _collider)
    {
        // Only trigger the event if the right tagged object is allowed to
        if((_collider.tag == _tagName) && (_shouldLeavingTrigger))
        {
            ActorDataEvent.Trigger();
        }
    }
}
