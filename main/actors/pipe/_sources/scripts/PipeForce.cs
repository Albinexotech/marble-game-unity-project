using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PipeForce : Actor
{
    // Variables
    // How much force does the hose give?
    [SerializeField]
    private float _thrust;
    // What type of force should the hose apply?
    [SerializeField]
    private ForceMode _forceMode;
    // Particles the hose emits
    [SerializeField]
    private ParticleSystem _particles;

    // Functions
    // When object is started. Overrides the base function
    protected override void Start()
    {
        base.Start();
        ResetParticles();
    }
    // Set particle state to what it should be
    void ResetParticles()
    {
        // Stop the particles if off
        if(IsActorActive == false)
        {
            _particles.Stop();
        }
        else
        {
            _particles.Play();
        }
    }
    // When an object enters the collider
    void OnTriggerStay(Collider _collider)
    {
        // If the hose is on, push Rigidbodies
        if(IsActorActive == true)
        {
            _collider.attachedRigidbody.AddForce(transform.up * _thrust, _forceMode);
        }
    }
    // When the button is pushed, invert the value
    public override void OnStateChanged()
    {
        base.OnStateChanged();
        ResetParticles();
    }
    // Called when the player respawns
    public override void OnPlayerRespawn()
    {
        // Call base function, then reset particles
        base.OnPlayerRespawn();
        ResetParticles();
    }
}
