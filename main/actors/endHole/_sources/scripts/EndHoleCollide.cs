using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndHoleCollide : MonoBehaviour
{
    // Fields / Properties
    // Particles to play
    [SerializeField]
    private ParticleSystem _particles;
    // Max velocity a player can travel before they are considered "safe" in the hole
    [SerializeField]
    private float _maxSpeed;
    // Data event to call to end the level
    [SerializeField]
    private DataEvent _endEvent;
    // SceneManager ScriptableObject to tell to load the next level
    [SerializeField]
    private DataSceneManager _sceneManager;
    // Boolean to ensure the particles only play once
    private bool _hasParticlesPlayed = false;
    
    // Methods
    // Called when something enters the hole
    void OnTriggerStay(Collider _collider)
    {
        // If the player enters the hole and stays still, the particles play and the next scene event is played
        if((_collider.tag == "Player") && (_collider.attachedRigidbody.velocity.magnitude <= _maxSpeed) && (!_hasParticlesPlayed))
        {
            _particles.Play();
            _hasParticlesPlayed = true;
            _endEvent.Trigger();
            // Start the coroutine
            StartCoroutine(TimerLevelEnd());
        }
    }
    // Coroutine that tells the game to wait 5 seconds, then load the next scene
    IEnumerator TimerLevelEnd()
    {
        yield return new WaitForSeconds(5.0f);
        _sceneManager.LoadNextScene();
    }
}
